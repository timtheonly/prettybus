import sys
import datetime
from git import Repo
from termcolor import cprint
from fabric.utils import abort
from fabric.api import local, task
from fabric.context_managers import lcd
from fabric.contrib.console import confirm
from fabric.tasks import execute

text_colours = {
  'info': 'cyan',
  'warn': 'yellow',
  'error': 'red',
}

repo = Repo('/Users/Daniel/projects/prettybus')
if repo.is_dirty():
    cprint(
        'Repo is dirty. Commit or stash your changes.',
        text_colours['error']
    )
    sys.exit()

@task
def tag():
    sha = repo.head.commit.hexsha
    shortSha = repo.git.rev_parse(sha, short=6)
    timestamp = datetime.datetime.now().strftime('%Y%m%d%H')
    tagName = "{}_{}".format(shortSha, timestamp)
    if tagName not in repo.tags:
        repo.create_tag(tagName)
        cprint("Created new tag: {}".format(tagName), text_colours['info'])
    else:
        cprint(
            "Tag \"{}\" already exists".format(tagName),
            text_colours['warn']
        )
    cprint("Pushing tag up to origin", 'cyan')
    tag = repo.tags[tagName]
    origin = repo.remotes.origin
    origin.push(tag)


@task
def build():
    cprint("Running build", text_colours['info'])
    local("node build/build.js")
    cprint("Build complete", text_colours['info'])


@task
def deploy():
    # generate changelog
    tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
    current_tag = tags[-1].name
    previous_tag = tags[-2].name

    cprint(
        "Generating diff {}...{}".format(current_tag, previous_tag),
        text_colours['info']
    )
    changelog = repo.git.log(
      '--oneline',
      '--decorate',
      '{}...{}'.format(current_tag, previous_tag)
    )
    cprint("Changelog: {}".format(changelog), text_colours['info'])
    if not confirm("Deploy these changes ?", default=True):
        abort("aborting at users request")

    cprint("Copying files to web repo", text_colours['info'])
    with lcd('/Users/Daniel/projects/prettybus-web'):
        local('pwd')
        local('rm -rf static/* *.eot *.svg *.woff2 *.woff *.ttf index.html')
        local('cp -r /Users/Daniel/projects/prettybus/dist/* .')

    # commit the changes
    cprint("Deploying changes", text_colours['info'])
    web_repo = Repo('/Users/Daniel/projects/prettybus-web')
    if not web_repo.is_dirty():
        abort("Something went bad web repo has no changes")
    web_repo.git.add(A=True)
    web_repo.index.commit('{}\n\n{}'.format(current_tag, changelog))
    tag = web_repo.create_tag(current_tag)
    web_repo.remotes.origin.push(tag)
    web_repo.remotes.origin.push()
    cprint("Changes Deployed!", text_colours['info'])


@task
def deploy_now():
    cprint("Deploying now!")
    execute(build)
    execute(deploy)
    execute(tag)
