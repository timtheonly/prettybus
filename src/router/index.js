import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import About from '@/views/About'
import ViewStop from '@/views/ViewStop'
import ListStops from '@/views/ListStops'

Vue.use(Router)

var routeMap = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/stops',
    name: 'Stops',
    component: ListStops
  },
  {
    path: '/stop',
    name: 'Stop',
    component: ViewStop
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }
]

// define the SPA routes
// each route is a component
var router = new Router({
  routes: routeMap
})

export {router, routeMap}
