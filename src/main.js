// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import {router, routeMap} from './router'
import VueAnalytics from 'vue-analytics'
require('font-awesome-sass-loader')
Vue.config.productionTip = false

if (process.env.ANALYTICS) {
  Vue.use(VueAnalytics, {
    id: 'UA-98635393-1',
    router });

  /* eslint-disable */
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MKD2QQM');
  /* eslint-enable */
}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: function () {
    return {
      routeMap: routeMap // give the app the routeMap so it can pass it to the component
    }
  },
  template: '<App :routeMap="routeMap" />',
  components: { App }
})
