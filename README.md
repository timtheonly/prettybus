# prettybus

A project that aims to create a pretty interface for Dublin bus RTPI (Realtime Passenger Information). 

Wanna see it in action check it out [here](https://timtheonly.github.io/prettybus/?utm_source=gitlab&utm_campaign=readme).

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



